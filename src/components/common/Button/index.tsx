import * as React from 'react';
import { ButtonPropsType } from './types';

const Button = ({
  className,
  dataField,
  type,
  name,
  onClick,
  text,
  disabled = false,
  hidden = false
}: ButtonPropsType) => (
  <button
    data-field={dataField}
    className={className}
    disabled={disabled}
    type={type}
    id={name}
    name={name}
    onClick={onClick}
    hidden={hidden}
  >
    {text}
  </button>
);

export default Button;
