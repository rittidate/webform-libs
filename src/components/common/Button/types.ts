export interface ButtonPropsType {
  className?: string;
  dataField?: string;
  disabled?: boolean;
  name?: string;
  text?: string;
  type?: any;
  hidden?: boolean;
  onClick: (e: any, data?: any) => void;
}
