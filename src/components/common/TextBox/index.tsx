import * as React from 'react';
import { TextBoxPropsType } from './types';

const TextBox = ({
  classNameLabel,
  idLabel,
  classNameInput,
  idInput,
  dataField,
  label,
  type,
  placeholder,
  name,
  dataType,
  value,
  onChange,
  onKeyDown,
  onFocus,
  onClick,
  isHidden = false,
  onBlur,
  maxLength = 255,
  autoComplete = 'off',
  warningMsg,
  inputMode,
  required = false,
  hasSuffix = false,
  suffixVal,
  disabled = false
}: TextBoxPropsType) => (
  <label
    className={classNameLabel}
    id={idLabel}
    hidden={isHidden}
    data-field={dataField}
  >
    {label}
    <input
      type={type}
      id={idInput || name}
      name={name}
      data-type={dataType}
      className={classNameInput}
      value={value}
      placeholder={placeholder}
      onKeyDown={onKeyDown}
      onChange={onChange}
      onBlur={onBlur}
      onFocus={onFocus}
      onClick={onClick}
      max-length={maxLength}
      autoComplete={autoComplete}
      required={required}
      disabled={disabled}
      data-warning-msg={warningMsg}
      inputMode={inputMode}
    />
    {hasSuffix && <span className='suffix-value'>{suffixVal}</span>}
  </label>
);

export default TextBox;
