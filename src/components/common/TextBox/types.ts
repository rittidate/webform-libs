export interface TextBoxPropsType {
  classNameLabel: string;
  idLabel?: string;
  classNameInput?: string;
  idInput?: string;
  dataField?: string;
  label?: string;
  type?: string;
  placeholder: string;
  name: string;
  dataType?: string;
  value: string;
  isHidden?: boolean;
  onChange: (e: any) => void;
  onBlur?: (e: any) => void;
  onFocus?: (e: any) => void;
  onClick?: (e: any) => void;
  onKeyDown?: (e: any) => void;
  maxLength?: number;
  autoComplete?: string;
  required?: boolean;
  hasSuffix?: boolean;
  suffixVal?: string;
  disabled?: boolean;
  warningMsg?: string;
  inputMode?: any;
}
