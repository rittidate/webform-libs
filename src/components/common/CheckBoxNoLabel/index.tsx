import * as React from 'react';
import { CheckBoxNoLabelPropsType } from './types';

const CheckBoxNoLabel = ({
  className,
  id,
  name,
  checked,
  onChange,
  onClick,
  label,
  descriptionVal,
  dataField,
  checkBoxDesc,
  checkBoxClassName
}: CheckBoxNoLabelPropsType) => (
  <div className={className} data-field={dataField}>
    <input
      type='checkbox'
      id={id}
      name={name}
      className={checkBoxClassName}
      checked={checked}
      onChange={onChange}
      onClick={onClick}
    />
    {label}
    {!!descriptionVal && <p className='description'>{descriptionVal}</p>}
    {!!checkBoxDesc && (
      <span dangerouslySetInnerHTML={{ __html: checkBoxDesc }} />
    )}
  </div>
);

export default CheckBoxNoLabel;
