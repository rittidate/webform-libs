export interface CheckBoxNoLabelPropsType {
  className: string;
  id?: string;
  name: string;
  value?: string;
  checked: boolean;
  onChange: (e: any) => void;
  onClick?: (e: any) => void;
  label: string;
  descriptionVal?: string;
  dataField?: string;
  checkBoxDesc?: string;
  checkBoxClassName?: string;
}
