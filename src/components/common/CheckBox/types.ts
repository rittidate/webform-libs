export interface CheckBoxPropsType {
  className: string;
  name: string;
  value?: string;
  checked: boolean;
  onChange: (e) => void;
  onClick?: (e) => void;
  label: string;
  descriptionVal?: string;
  dataField?: string;
  checkBoxDesc?: string;
  hidden?: boolean;
}
