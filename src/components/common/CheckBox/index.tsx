import * as React from 'react';
import { CheckBoxPropsType } from './types';

const CheckBox = ({
  className,
  name,
  value,
  checked,
  onChange,
  onClick,
  label,
  descriptionVal,
  dataField,
  checkBoxDesc,
  hidden = false
}: CheckBoxPropsType) => (
  <label className={className} data-field={dataField} hidden={hidden}>
    <input
      type='checkbox'
      name={name}
      value={value}
      checked={checked}
      onChange={onChange}
      onClick={onClick}
    />
    {label}
    {!!descriptionVal && <p className='description'>{descriptionVal}</p>}
    {!!checkBoxDesc && (
      <span dangerouslySetInnerHTML={{ __html: checkBoxDesc }} />
    )}
  </label>
);

export default CheckBox;
