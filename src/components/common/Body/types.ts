export interface BodyPropsType {
  className?: string;
  id?: string;
  onScroll?: () => void;
  dataField?: string;
  children: any;
}
