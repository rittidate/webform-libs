import * as React from 'react';
import { BodyPropsType } from './types';

const Body: React.FunctionComponent<BodyPropsType> = ({
  className,
  id,
  children,
  onScroll,
  dataField
}) => (
  <div className={className} data-field={dataField} id={id} onScroll={onScroll}>
    {children}
  </div>
);

export default Body;
