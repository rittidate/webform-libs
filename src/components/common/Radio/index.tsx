import * as React from 'react';
import { RadioPropsType } from './types';

const Radio: React.FunctionComponent<RadioPropsType> = ({
  className,
  name,
  value,
  checked,
  onChange,
  title,
  hidden = false,
  descriptionVal,
  dataFieldDescription
}) => (
  <p className='row' hidden={hidden}>
    <label className={className} hidden={hidden}>
      <input
        type='radio'
        name={name}
        value={value}
        checked={checked}
        onChange={onChange}
      />
      {title}
    </label>
    {!!descriptionVal && (
      <p data-field={dataFieldDescription} className='description'>
        {descriptionVal}
      </p>
    )}
  </p>
);

export default Radio;
