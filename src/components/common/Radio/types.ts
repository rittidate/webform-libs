export interface RadioPropsType {
  className?: string;
  name: string;
  value: string;
  checked: boolean;
  onChange: (e: any) => void;
  title: string;
  hidden?: boolean;
  descriptionVal?: string;
  dataFieldDescription?: string;
}
