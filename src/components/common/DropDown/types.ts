export interface DropDownPropsType {
  className: string;
  label?: string;
  id: string;
  name: string;
  value: string;
  onChange: (e: any) => void;
  onBlur: (e: any) => void;
  onFocus?: (e: any) => void;
  required?: boolean;
  placeholder?: string;
  placeholderCode?: string;
  hidden?: boolean;
  warningMsg?: string;
  options: any[];
  optionName: string;
  optionValue: string;
  isOptionEmpty?: boolean;
  disabled?: boolean;
  dataField?: string;
  labelFlying?: boolean;
}
