import * as React from 'react';
import { DropDownPropsType } from './types';

const DropDown: React.FunctionComponent<DropDownPropsType> = ({
  className,
  label,
  id,
  name,
  value,
  dataField,
  onChange,
  onBlur,
  onFocus = () => {},
  required,
  placeholder,
  placeholderCode,
  hidden,
  warningMsg,
  options,
  optionName,
  optionValue,
  isOptionEmpty = false,
  disabled = false,
  labelFlying = true
}) => (
  <label className={className} hidden={hidden} data-field={dataField}>
    {labelFlying ? '' : label}
    <select
      id={id}
      name={name}
      value={value}
      selected-value={value}
      onChange={onChange}
      onFocus={onFocus}
      onBlur={onBlur}
      required={required}
      data-warning-msg={warningMsg}
      disabled={disabled}
    >
      {isOptionEmpty && <option value='' hidden />}
      {placeholder && (
        <option value='' disabled>
          {placeholder}
        </option>
      )}
      {options &&
        options.map((item, index) => (
          <option
            key={index}
            value={item[optionName] === placeholderCode ? '' : item[optionName]}
            disabled={item[optionName] === placeholderCode}
          >
            {item[optionValue]}
          </option>
        ))}
    </select>
    <span className='select-label' hidden={!labelFlying}>
      {label}
    </span>
  </label>
);

export default DropDown;
