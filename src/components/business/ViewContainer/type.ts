export interface ViewContainerPropsType {
  children: any;
  id: string;
  modelName?: string;
  className?: string;
  name: string;
  setFormRef: any;
  classNameForm?: string;
}
