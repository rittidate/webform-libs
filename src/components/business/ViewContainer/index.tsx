import * as React from 'react';
import { ViewContainerPropsType } from './type';

const ViewContainer = ({
  className,
  id,
  name,
  children,
  setFormRef,
  modelName = '',
  classNameForm
}: ViewContainerPropsType) => (
  <div className={`view-container ${className}`}>
    <form
      id={id}
      name={name}
      model-name={modelName}
      ref={setFormRef}
      className={classNameForm}
    >
      {children}
    </form>
  </div>
);

export default ViewContainer;
