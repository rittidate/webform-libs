import { actionsResourceState } from '../../actions/ResourceStateAction';

type actionsTypeResourceState = typeof actionsResourceState;

export type ResourceStateAction = ReturnType<
  actionsTypeResourceState[keyof actionsTypeResourceState]
>;
