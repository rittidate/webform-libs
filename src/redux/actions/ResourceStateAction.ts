import { ResourceStateActionType } from '../types/enums/ResourceStateActionType';
import { ReducerActionType } from '../helpers/ReducerActionType';

function createAction<T extends ReducerActionType<ResourceStateActionType>>(
  d: T
): T {
  return d;
}

export const convertResouceState = (data: any) =>
  createAction({
    type: ResourceStateActionType.CONVERT_RESOURCE_STATE,
    payload: data
  });

export const actionsResourceState = {
  convertResouceState
};
