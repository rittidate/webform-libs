export interface ValueCode {
  value: string;
  code: string;
}
