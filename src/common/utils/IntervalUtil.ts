export class IntervalUtil {
  private static interval: any = null;

  public static createInterval(callback: () => any, time = 20000) {
    if (!this.interval) {
      this.interval = setInterval(callback, time);
    }
  }

  public static clearInterval() {
    if (this.interval) {
      this.interval = clearInterval(this.interval);
    }
  }
}
