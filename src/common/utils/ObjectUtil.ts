export class ObjectUtil {
  public static trimObject = (obj) => {
    Object.keys(obj).forEach((key) => {
      const v = obj[key];
      if (v) {
        if (typeof v === 'string') {
          const v2 = v.trim();
          if (v2 !== v) {
            obj[key] = v2;
          }
        } else if (typeof v === 'object') {
          ObjectUtil.trimObject(obj[key]);
        }
      }
    });
    return obj;
  };
}
