export class MaskUtil {
  public static accountFormatNo = (
    accountNo: string,
    showAccountNo = false
  ) => {
    let result = accountNo.replace(
      /(\d{3})(\d{1})(\d{5})(\d{1,})/,
      '$1-$2-$3-$4'
    );
    if (!showAccountNo) {
      result =
        result.slice(0, 7).replace(/\d/g, 'x') +
        result.slice(7, 12) +
        result.slice(12).replace(/\d{1,}/g, 'x');
    }
    return result;
  };

  public static mobileFormatNo = (mobileNo: string, showMobileNo = false) => {
    let result = mobileNo.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
    if (!showMobileNo) {
      result = result.slice(0, 7).replace(/\d/g, 'x') + result.slice(7);
    }
    return result;
  };
}
