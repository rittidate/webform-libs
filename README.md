# webform-components-libs

> components for webform

[![NPM](https://img.shields.io/npm/v/webform-components-libs.svg)](https://www.npmjs.com/package/webform-components-libs) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save webform-components-libs
```

## Usage

```tsx
import React, { Component } from 'react'

import MyComponent from 'webform-components-libs'
import 'webform-components-libs/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [](https://github.com/)
